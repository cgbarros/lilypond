FROM ubuntu:18.04 as build
RUN apt-get update && apt-get install --no-install-recommends -y \
        binutils \
        ca-certificates \
        gcc \
        libc-dev \
        libgmp-dev \
        libltdl-dev \
        make \
        wget \
    && true
# Download and extract urw-base35-fonts. Do this is in a build container to
# not distribute the whole archive, only the *.otf files.
# Do not use ADD which doesn't cache the downloaded archive. Once released, it
# will never change.
RUN wget -q https://github.com/ArtifexSoftware/urw-base35-fonts/archive/20170801.1.tar.gz \
    && mkdir -p /usr/share/fonts/otf/ && tar -C /usr/share/fonts/otf/ \
        -xf /20170801.1.tar.gz --strip-components=2 --wildcards '*/fonts/*.otf'
# Download and build Guile 1.8.8, there's no package in Ubuntu 18.04.
RUN wget -q https://ftp.gnu.org/gnu/guile/guile-1.8.8.tar.gz \
    && tar xf guile-1.8.8.tar.gz && mkdir build-guile1.8 && cd build-guile1.8 \
    && /guile-1.8.8/configure --prefix=/usr --disable-error-on-warning \
    && make -j$(nproc) && make install-strip DESTDIR=/install-guile1.8

FROM ubuntu:18.04
COPY --from=build /usr/share/fonts/otf/ /usr/share/fonts/otf/
COPY --from=build /install-guile1.8/ /
RUN apt-get update && apt-get --no-install-recommends install -y \
        autoconf \
        bison \
        flex \
        fontconfig \
        fontforge \
        fonts-texgyre \
        g++ \
        gettext \
        ghostscript \
        imagemagick \
        libfl-dev \
        libfontconfig1-dev \
        libfreetype6-dev \
        libglib2.0-dev \
        libgmp-dev \
        libgs-dev \
        libltdl-dev \
        libpango1.0-dev \
        make \
        netpbm \
        perl \
        pkg-config \
        python3 \
        rsync \
        texi2html \
        texinfo \
        texlive-binaries \
        texlive-fonts-recommended \
        texlive-lang-cyrillic \
        texlive-metapost \
        texlive-plain-generic \
        zip \
    && rm -rf /var/lib/apt/lists/* \
    && rm -rf /usr/share/doc /usr/share/man

# Drop root user inside the container.
USER nobody
